class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token

  protected

    def authenticate_user
      authenticate(self) do |token, options|
        user = User.find_by(id: options["id"])

        if user.blank?
          render json: {
            errors: { base: ["Invalid token"] }
          }, status: :unauthorized
        else
          valid = ActiveSupport::SecurityUtils.secure_compare(token, user.token)

          if !valid
            render json: {
              errors: { base: ["Invalid token"] }
            }, status: :unauthorized
          else
            @current_user = user
            return true
          end
        end
      end
    end
end
