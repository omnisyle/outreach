class Api::V1::RelationshipsController < ApplicationController

  before_action :authenticate_user, only: [:create]
  before_action :set_user, only: [:show]

  def show
    friends_list = RelationshipQueryService.friends_of(@user.id)
    render json: friends_list, root: "users"
  end

  def create
    form = AddFriendForm.new(
      user_id: @current_user.id,
      other_user_id: create_params[:friend_id]
    )

    if form.save
      head 201
    else
      render json: form.errors.messages,
        status: :unprocessable_entity
    end
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def create_params
      params.permit(:friend_id)
    end
end
