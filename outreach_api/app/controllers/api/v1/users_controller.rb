class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /api/v1/users
  def index
    @users = User.includes(:headings).all

    render json: @users,
      each_serializer: Api::V1::UserSerializer
  end

  # GET /api/v1/users/1
  def show
    render json: @user,
      serializer: Api::V1::UserSerializer
  end

  # POST /api/v1/users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user,
        serializer: Api::V1::UserSerializer,
        status: :created
    else
      render json: @user.errors,
        status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users/1
  def update
    if @user.update(user_params)
      render json: @user,
        serializer: Api::V1::UserSerializer
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private
    def set_user
      @user = User.includes(:headings).find(params[:id])
    end

    def user_params
      params.
        require(:user).
        permit(
          :first_name,
          :last_name,
          :email,
          :website,
          :password,
          :password_confirmation
        )
    end
end
