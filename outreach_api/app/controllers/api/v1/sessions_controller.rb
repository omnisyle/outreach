class Api::V1::SessionsController < ApplicationController

  def create
    user = User.
      find_by(email: session_params[:email]).
      try(:authenticate, session_params[:password])

    return invalid_request unless user.present?
    authorization_value = encode_credentials(user.token, { id: user.id })

    response.set_header("Authorization", authorization_value)

    render json: {}, status: :ok
  end

  private

    def invalid_request
      render json: { errors: { base: ["Invalid email or password"] } },
        status: :unauthorized
    end

    def session_params
      params.require(:user).permit(:email, :password)
    end
end
