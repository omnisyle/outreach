class Api::V1::ExpertsFinderController < ApplicationController
  before_action :authenticate_user, only: [:search]

  def search
    results = ExpertFinderService.find(
      search_phrase: search_params[:q],
      user: @current_user
    )

    render json: results,
      root: "results",
      meta: {
        total_count: results.length
      }
  end

  private
    def search_params
      params.permit(:q)
    end
end
