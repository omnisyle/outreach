require "open-uri"

class WebScrapperService

  def initialize(url:)
    @doc = Nokogiri::HTML(open(url))
  end

  def get_content(element)
    heading_contents = @doc.css(element).map do |ele|
      ele.content.strip
    end

    heading_contents.compact
  end
end
