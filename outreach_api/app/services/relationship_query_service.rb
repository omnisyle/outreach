class RelationshipQueryService

  def self.friends_of(id)
    User.
      joins("
        inner JOIN relationships \
        ON relationships.user_id = users.id \
        or relationships.other_user_id = users.id
      ").
      where("
        (relationships.user_id = ? \
          or relationships.other_user_id = ?) \
        and users.id <> ? \
      ", id, id, id).
      distinct
  end
end
