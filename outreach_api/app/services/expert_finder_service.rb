module ExpertFinderService

  def self.find(search_phrase:, user:, depth: 3)
    ExpertSearchEngine.new(user, depth).find(search_phrase)
  end

  class ExpertSearchResult
    include ActiveModel::Model
    include ActiveModel::Serialization

    attr_accessor :users, :heading
  end

  class ExpertSearchUser
    include ActiveModel::Model
    include ActiveModel::Serialization

    attr_accessor :user, :order
  end

  class ExpertSearchEngine

    def initialize(user, depth = 3)
      @user = user
      @user_friends = RelationshipQueryService.friends_of(@user.id)
      @max_depth = depth
    end

    def find(search_phrase)
      return [] if search_phrase.blank?

      headings = Heading.search(search_phrase)
      return [] if headings.blank?

      user_friend_ids = @user_friends.pluck(:id)

      filtered_headings = headings.select do |heading|
        !user_friend_ids.include?(heading.user_id) &&
          heading.user_id != @user.id
      end

      return [] if filtered_headings.blank?

      # map the remaining headings for quick lookup
      heading_user_hash = filtered_headings.reduce({}) do |result, heading|
        result[heading.user_id] = heading
        result
      end

      results = find_path(heading_user_hash)
      format_node_results(results)
    end

    private

      def format_node_results(results)
        formatted_results = []

        results.each do |result|
          node = result[:node]
          node_values = [node.value]
          parent = node.parent

          while parent.present?
            node_values.push(parent.value)
            parent = parent.parent
          end

          node_values.reverse!

          users = User.where(id: node_values)

          values = users.map do |user|
            ExpertSearchUser.new(
              user: user,
              order: node_values.index(user.id)
            )
          end

          search_result = ExpertSearchResult.new(
            users: values,
            heading: result[:heading]
          )
          formatted_results.push(search_result)
        end

        formatted_results
      end

      def find_path(headings)
        # initialize variables and root node
        root = FriendNode.new(@user.id)
        depth = 0
        layers = { depth => { root.value => root }}
        visited = []
        previous_values = [root.value]
        results = []

        # build friend tree
        while depth < @max_depth
          previous_layer = layers[depth] || {}

          # find friends of previous layer's friends
          ids = previous_values - visited
          new_friends = find_next_friend_layer(ids)

          # reset layer values
          previous_values = []

          layer_cache = {}

          new_friends.each do |friend|

            # find_previous_node
            previous_node = previous_layer[friend.user_id] ||
              previous_layer[friend.other_user_id]

            # init new node
            new_node_id = previous_node.value == friend.user_id ?
              friend.other_user_id :
              friend.user_id

            node = FriendNode.new(new_node_id)

            previous_values.push(new_node_id)
            visited.push(previous_node.value)
            previous_node.add(node)
            layer_cache[node.value] = node

            # check for found
            if headings[node.value].present?
              results.push({
                node: node,
                heading: headings[node.value]
              })
            end
          end

          depth += 1
          layers[depth] = layer_cache
        end

        results
      end

      def find_next_friend_layer(user_ids)
        Relationship.
          where(user_id: user_ids).
          or(Relationship.where(other_user_id: user_ids))
      end
  end

  class FriendNode

    attr_accessor :value, :parent

    def initialize(value)
      @value = value
      @parent = nil
      @children = []
    end

    def add(node)
      @children.push(node)
      node.parent = self
    end
  end
  private_constant :FriendNode
end
