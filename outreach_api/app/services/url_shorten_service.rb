class UrlShortenService

  def initialize
    token = Rails.application.credentials.bitly[:token]
    @client = Bitly::API::Client.new(token: token)
  end

  def shorten(url:)
    bitlink = @client.shorten(long_url: url)
    bitlink.link
  end
end
