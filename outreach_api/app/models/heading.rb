class Heading < ApplicationRecord
  include ActiveRecord::Sanitization
  belongs_to :user

  validates :content, presence: true

  before_save :generate_content_tokens, if: :content_changed?

  def self.search(phrase)
    query = phrase.split(" ").join("&")
    where("content_tokens @@ to_tsquery(?)", query)
  end

  def generate_content_tokens
    return false if self.content.blank?
    sql = Heading.sanitize_sql(["select to_tsvector(?);", self.content])

    results = ActiveRecord::Base.connection.exec_query(sql)
    self.content_tokens = results.first["to_tsvector"]
  end
end
