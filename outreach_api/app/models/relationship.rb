class Relationship < ApplicationRecord
  validates :user_id, :other_user_id, presence: true
  validates :user_id, uniqueness: { scope: :other_user_id }
  validate :order_of_ids

  private

    def order_of_ids
      return true if self.user_id.blank? || self.other_user_id.blank?
      return true if self.user_id < self.other_user_id
      errors.add(:base, "invalid user ids")
      false
    end
end
