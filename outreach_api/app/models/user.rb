class User < ApplicationRecord
  include FriendlyId

  friendly_id :slug_candidates, use: :slugged
  has_secure_password :password, validations: true

  validates :first_name,
    :last_name,
    presence: true

  validates :password_confirmation, presence: true, on: :create

  validates :email,
    presence: true,
    uniqueness: true,
    format: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :website, presence: true,
    format: /(\b(https?):\/\/)[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+[-A-Za-z0-9+&@#\/%=~_|]/

  has_many :headings, dependent: :destroy
  accepts_nested_attributes_for :headings

  before_create :generate_token
  before_save :shorten_website_url
  before_save :scrape_website

  def slug_candidates
    [
      :first_name,
      [:first_name, :last_name],
    ]
  end

  def shorten_website_url
    return false if self.website.blank? || !website_changed?

    service = UrlShortenService.new
    shorten_url = service.shorten(url: self.website)

    self.website_shortlink = shorten_url
  end

  def scrape_website
    return false if self.website.blank? || !website_changed?

    service = WebScrapperService.new(url: self.website)
    heading_contents = [
      service.get_content("h1"),
      service.get_content("h2"),
      service.get_content("h3")
    ].flatten

    new_headings = heading_contents.map do |content|
      { content: content }
    end

    self.headings.each(&:destroy)
    self.headings_attributes = new_headings
  end

  def generate_token
    self.token = token_loop
  end

  private

    def token_loop
      loop do
        token = SecureRandom.hex(13)
        unless User.where(token: token).any?
          break token
        end
      end
    end
end
