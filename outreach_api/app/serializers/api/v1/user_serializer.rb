class Api::V1::UserSerializer < ActiveModel::Serializer
  has_many :headings, each_serializer: Api::V1::HeadingSerializer
  attributes :id,
    :slug,
    :first_name,
    :last_name,
    :website,
    :email,
    :website_shortlink
end
