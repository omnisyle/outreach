class Api::V1::ExpertSearchResultSerializer < ActiveModel::Serializer
  has_one :heading, serializer: Api::V1::HeadingSerializer
  has_many :users, each_serializer: Api::V1::ExpertSearchUserSerializer
end
