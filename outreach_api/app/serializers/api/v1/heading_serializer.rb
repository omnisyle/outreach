class Api::V1::HeadingSerializer < ActiveModel::Serializer
  attributes :content,
    :user_id,
    :created_at
end
