class Api::V1::ExpertSearchUserSerializer < ActiveModel::Serializer
  attributes :order, :user

  def user
    Api::V1::UserSerializer.new(object.user).as_json
  end
end
