class AddFriendForm
  include ActiveModel::Model

  attr_accessor :user_id,
    :other_user_id

  validates :user_id, :other_user_id, presence: true
  validate :other_user_valid

  def save
    if valid?
      return persist
    end

    false
  end

  private

    def other_user_valid
      u = User.find_by(id: self.other_user_id)

      return true if u.present?

      self.errors.add(:other_user_id, "invalid user")
      false
    end

    def persist
      user_one_id = user_id < other_user_id ? user_id : other_user_id
      user_two_id = user_id < other_user_id ? other_user_id : user_id

      relationship = Relationship.new(
        user_id: user_one_id,
        other_user_id: user_two_id
      )

      return true if relationship.save

      copy_errors(relationship)

      false
    end

    def copy_errors(resource)
      return if resource.blank?
      if resource.errors.present?
        resource.errors.messages.each do |key, value|
          errors.add(key, value.join(", "))
        end
      end
    end
end
