class AddUniqueIndexToRelationships < ActiveRecord::Migration[6.0]
  def change
    add_index :relationships, [:user_id, :other_user_id], unique: true
  end
end
