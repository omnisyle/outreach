class CreateRelationships < ActiveRecord::Migration[6.0]
  def change
    create_table :relationships do |t|
      t.references :user, null: false, foreign_key: true
      t.bigint :other_user_id, null: false

      t.timestamps
    end

    add_foreign_key :relationships, :users, column: :other_user_id
  end
end
