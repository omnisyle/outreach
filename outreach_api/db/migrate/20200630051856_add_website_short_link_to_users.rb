class AddWebsiteShortLinkToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :website_shortlink, :string
  end
end
