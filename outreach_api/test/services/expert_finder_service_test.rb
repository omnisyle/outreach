require "test_helper"

class ExpertFinderServiceTest < ActiveSupport::TestCase

  setup do
    # setup
    @jon = users(:jon)
    @tim = users(:tim)
    @jen = users(:jen)

    friend_form = AddFriendForm.new(
      user_id: @jon.id,
      other_user_id: @tim.id
    )
    assert friend_form.save

    friend_form = AddFriendForm.new(
      user_id: @tim.id,
      other_user_id: @jen.id
    )

    assert friend_form.save
  end

  test "find 2nd degree friend" do
    heading = Heading.new(user: @jen, content: "quick brown fox")
    assert heading.save

    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    assert_not_nil results
    assert_equal 1, results.length
  end

  test "find 3rd degree friend" do
    haddock = users(:haddock)

    friend_form = AddFriendForm.new(
      user_id: @jen.id,
      other_user_id: haddock.id
    )

    assert friend_form.save

    heading = Heading.new(user: haddock, content: "quick brown fox")
    assert heading.save

    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    assert_not_nil results
    assert_equal 1, results.length
  end

  test "mutliple results" do
    haddock = users(:haddock)

    [haddock, @jen].each do |u|
      heading = Heading.new(user: u, content: "quick brown fox")
      assert heading.save
    end

    friend_form = AddFriendForm.new(
      user_id: @tim.id,
      other_user_id: haddock.id
    )

    assert friend_form.save

    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    # jon -> tim -> jen
    # jon -> tim -> haddock
    assert_not_nil results
    assert_equal 2, results.length
  end

  test "multiple results from two friends" do
    haddock = users(:haddock)
    tuna = users(:tuna)

    [tuna, @jen].each do |u|
      heading = Heading.new(user: u, content: "quick brown fox")
      assert heading.save
    end

    friend_form = AddFriendForm.new(
      user_id: @jon.id,
      other_user_id: haddock.id
    )
    assert friend_form.save

    friend_form = AddFriendForm.new(
      user_id: tuna.id,
      other_user_id: haddock.id
    )

    assert friend_form.save

    # jon -> tim -> jen
    # jon -> haddock -> tuna
    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    assert_not_nil results
    assert_equal 2, results.length
  end

  test "multiple headings one relationship" do
    haddock = users(:haddock)
    tuna = users(:tuna)

    [tuna, @jen].each do |u|
      heading = Heading.new(user: u, content: "quick brown fox")
      assert heading.save
    end

    friend_form = AddFriendForm.new(
      user_id: @jon.id,
      other_user_id: haddock.id
    )
    assert friend_form.save

    # jon -> tim -> jen
    # jon -> haddock
    # tuna is lonely
    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    assert_not_nil results
    assert_equal 1, results.length
  end

  test "friend has the same article" do
    haddock = users(:haddock)

    [haddock, @jen].each do |u|
      heading = Heading.new(user: u, content: "quick brown fox")
      assert heading.save
    end

    friend_form = AddFriendForm.new(
      user_id: @jon.id,
      other_user_id: haddock.id
    )

    assert friend_form.save

    # jon -> tim -> jen
    # jon -> haddock
    results = ExpertFinderService.find(
      search_phrase: "brown fox",
      user: @jon
    )

    assert_not_nil results
    assert_equal 1, results.length
  end
end
