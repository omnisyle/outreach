require "test_helper"

class RelationshipQueryServiceTest < ActiveSupport::TestCase

  test "friends_of" do
    jon = users(:jon)
    tim = users(:tim)
    jen = users(:jen)

    friend_form = AddFriendForm.new(user_id: jon.id, other_user_id: tim.id)
    assert friend_form.save

    friend_form = AddFriendForm.new(user_id: jon.id, other_user_id: jen.id)
    assert friend_form.save

    friends = RelationshipQueryService.friends_of(jon.id)

    assert_includes friends, tim
    assert_includes friends, jen
    assert_equal 2, friends.count

    friends = RelationshipQueryService.friends_of(tim.id)

    assert_includes friends, jon
    assert_equal 1, friends.count
  end
end
