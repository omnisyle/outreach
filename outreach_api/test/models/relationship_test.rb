require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase

  test "validations" do
    relationship = Relationship.new
    assert_not relationship.save

    assert_includes relationship.errors.messages, :user_id
    assert_includes relationship.errors.messages, :other_user_id
  end

  test "validate order of ids" do
    user_one = users(:jon)
    user_two = users(:jen)

    user_one_id = user_one.id < user_two.id ? user_one.id : user_two.id
    user_two_id = user_one.id < user_two.id ? user_two.id : user_one.id

    relationship = Relationship.new(
      user_id: user_one_id,
      other_user_id: user_two_id,
    )

    assert relationship.save

    user_three = users(:tim)
    user_one_id = user_one.id < user_three.id ? user_one.id : user_three.id
    user_two_id = user_one.id < user_three.id ? user_three.id : user_one.id

    relationship = Relationship.new(
      user_id: user_two_id,
      other_user_id: user_one_id,
    )

    assert_not relationship.save
    assert_includes relationship.errors.messages, :base
  end

  test "uniqueness pair" do
    user_one = users(:jon)
    user_two = users(:jen)

    user_one_id = user_one.id < user_two.id ? user_one.id : user_two.id
    user_two_id = user_one.id < user_two.id ? user_two.id : user_one.id

    relationship = Relationship.new(
      user_id: user_one_id,
      other_user_id: user_two_id,
    )

    assert relationship.save

    relationship = Relationship.new(
      user_id: user_one_id,
      other_user_id: user_two_id,
    )

    assert_not relationship.save
  end
end
