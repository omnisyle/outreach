require "test_helper"
require "mocha/minitest"

class UserTest < ActiveSupport::TestCase

  setup do
    shorten_service_mock = mock
    shorten_service_mock.expects(:shorten).returns("https://bit.ly/short").at_most(1)
    UrlShortenService.stubs(:new).returns(shorten_service_mock)

    website_scrapper_mock = mock
    website_scrapper_mock.expects(:get_content).returns(["heading content"]).at_most(3)
    WebScrapperService.stubs(:new).returns(website_scrapper_mock)
  end

  test "should validate" do
    u = User.new

    assert_not u.save
    assert_includes u.errors.messages, :first_name
    assert_includes u.errors.messages, :last_name
    assert_includes u.errors.messages, :password
    assert_includes u.errors.messages, :password_confirmation
    assert_includes u.errors.messages, :website
    assert_includes u.errors.messages, :email
  end

  test "should confirm password" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "https://google.com",
      password: "asdfasdf"
    )

    assert_not u.save
    assert_includes u.errors.messages, :password_confirmation

    u.password_confirmation = "asdf"

    assert_not u.save
    assert_includes u.errors.messages, :password_confirmation
  end

  test "should save" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "https://google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert u.save
    assert_not_nil u.slug
  end

  test "validate email format" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "rightwrongformat.com",
      website: "https://google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert_not u.save
    assert_includes u.errors.messages, :email
  end

  test "validate website format" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert_not u.save
    assert_includes u.errors.messages, :website
  end

  test "generate shortlink" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "https://google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert u.save
    assert_not_nil u.slug
    assert_not_nil u.website_shortlink
  end

  test "scrape website" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "https://google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert u.save
    assert_not_empty u.reload.headings
  end

  test "generate_token" do
    u = User.new(
      first_name: "First",
      last_name: "Last",
      email: "right@format.com",
      website: "https://google.com",
      password: "asdfasdf",
      password_confirmation: "asdfasdf"
    )

    assert u.save
    assert_not_nil u.token
  end
end
