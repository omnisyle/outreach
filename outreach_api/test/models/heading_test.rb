require 'test_helper'

class HeadingTest < ActiveSupport::TestCase

  test "validations" do
    heading = Heading.new

    assert_not heading.save

    assert_includes heading.errors.messages, :content
  end

  test "valid" do
    user = users(:jon)
    heading = Heading.new(
      user: user,
      content: "The quick brown fox jump over the lazy dog",
    )

    assert heading.save
  end

  test "content_tokens generation" do
    user = users(:jon)
    heading = Heading.new(
      user: user,
      content: "The quick brown fox jump over the lazy dog",
    )

    assert heading.save
    assert_not_nil heading.content_tokens
  end

  test "search" do
    user = users(:jon)
    heading = Heading.new(
      user: user,
      content: "The quick brown fox jump over the lazy dog",
    )

    assert heading.save

    results = Heading.search("quick jump")
    assert_not_empty results

    results = Heading.search("cat")
    assert_empty results
  end
end
