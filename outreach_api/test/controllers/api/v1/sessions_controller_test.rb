require "test_helper"

class Api::V1::SessionsControllerTest < ActionDispatch::IntegrationTest

  test "create" do
    jon = users(:jon)

    post api_v1_sessions_url, params: {
      user: {
        email: jon.email,
        password: "password"
      }
    }

    assert_not_nil @response.headers["Authorization"]
    assert_includes @response.headers["Authorization"], jon.id.to_s
    assert_response :ok
  end
end
