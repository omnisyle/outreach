require 'test_helper'

class Api::V1::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:jon)
  end

  test "should get index" do
    get api_v1_users_url, as: :json
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post api_v1_users_url, params: {
        user: {
          first_name: "Test",
          last_name: "Last",
          email: "tes@email.com",
          password: "testtest",
          website: "https://google.com",
          password_confirmation: "testtest"
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should show user" do
    get api_v1_user_url(@user), as: :json
    assert_response :success
  end

  test "should update api_v1_user" do
    patch api_v1_user_url(@user), params: {
      user: {
        first_name: "hello",
        last_name: "world"
      }
    }, as: :json
    assert_response 200
  end
end
