require "test_helper"
require "mocha/minitest"

class Api::V1::ExpertsFinderControllerTest < ActionDispatch::IntegrationTest

  test "search" do
    user = users(:jon)
    authorization = ActionController::HttpAuthentication::Token.encode_credentials(
      user.token, { id: user.id }
    )

    heading = Heading.new(
      user: users(:jen),
      content: "test"
    )

    assert heading.save

    ExpertFinderService.expects(:find).returns(
      [
        ExpertFinderService::ExpertSearchResult.new(
          users: [

            ExpertFinderService::ExpertSearchUser.new({
              user: users(:jen),
              order: 2
            }),
            ExpertFinderService::ExpertSearchUser.new({
              user: users(:tim),
              order: 1
            }),
            ExpertFinderService::ExpertSearchUser.new({
              user: user,
              order: 0
            })
          ],
          heading: heading
        )
      ]
    )

    get api_v1_experts_search_url(user), params: {
      q: "test"
    }, headers: {
      "HTTP_AUTHORIZATION" => authorization
    }

    assert_response :ok
  end
end
