require "test_helper"

class Api::V1::RelationshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @jon = users(:jon)
    @tim = users(:tim)
    @jen = users(:jen)

    friend_form = AddFriendForm.new(user_id: @jon.id, other_user_id: @tim.id)
    assert friend_form.save

    friend_form = AddFriendForm.new(user_id: @jon.id, other_user_id: @jen.id)
    assert friend_form.save
  end

  test "show" do
    get api_v1_relationship_url(@jon), as: :json
    assert_response :ok
  end

  test "create" do
    haddock = users(:haddock)
    authorization = ActionController::HttpAuthentication::Token.encode_credentials(
      @jon.token, { id: @jon.id }
    )

    post api_v1_relationships_url, params: {
      friend_id: haddock.id
    }, headers: { "HTTP_AUTHORIZATION" => authorization },
    as: :json

    assert_response 201
  end

  test "create failed" do
    authorization = ActionController::HttpAuthentication::Token.encode_credentials(
      @jon.token, { id: @jon.id }
    )

    post api_v1_relationships_url, params: {
      id: @jon.id,
      friend_id: "random"
    }, as: :json,
    headers: {
      "HTTP_AUTHORIZATION" => authorization
    }

    assert_response :unprocessable_entity
  end
end
