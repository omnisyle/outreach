source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

gem 'rails', '~> 6.0.3', '>= 6.0.3.2'
gem 'pg', '~> 1.2', '>= 1.2.3'
gem 'puma', '~> 4.1'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# Use Active Model has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# For user slugs
gem 'friendly_id', '~> 5.1'

# Bitly API
gem 'bitly', '~> 2.0', '>= 2.0.1'
gem 'nokogiri', '~> 1.10', '>= 1.10.9'
gem 'active_model_serializers', '~> 0.10.10'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry', '~> 0.13.1'
  gem 'awesome_print', '~> 1.8'
  gem 'pry-byebug', '~> 3.9'
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'capistrano', '~> 3.14', '>= 3.14.1'
  gem 'capistrano-rails', '~> 1.5'
  gem 'capistrano-rvm', '~> 0.1.2'
end

group :test do
  gem 'mocha', '~> 1.11', '>= 1.11.2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
