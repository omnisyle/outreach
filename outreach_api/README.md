# OutReach

OutReach is a contact directory api. It has the following features:

- User creation
- User website scrapping of h1-h3 tags and store into **headings** table
- User website link shortening using **bitly API**
- User can add friend(s)
- User can search for an expert who has mutual friend with them by searching through **headings** table

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/609114f8e3b08fc296c2)

# API

```
Note: 🔒 means authenticated route
```

## Users

### GET /api/v1/users

List all user in the current directory

### GET /api/v1/users/:id

Show information of a user

### POST /api/v1/users

Create/register a user

- params - user[first_name] - user's first name
- params - user[last_name] - user's last name
- params - user[email] - user's email, must be unique
- params - user[website] - user's website
- params - user[password] - user's password
- params - user[password_confirmation] - user's password confirmation

## Relationships

### GET /api/v1/relationships/:id

Get a list of friends of a user with `:id`

### 🔒 POST /api/v1/relationships

Add a user as friend

- params - friend_id - id of the other user whom to be added as friend

## Sessions

### POST /api/v1/sessions

Sign in endpoint

- params - user[email] - email
- params - user[password] - password

Token is returned in `Authorization` header, it can be saved and used for future requests to other endpoints.

## Expert search

Search for an expert and return introduction path to the expert from the current user

### 🔒 GET /api/v1/experts_search

- query - q - phrase to search for

# Search algorithm

To search for user's path to an expert, I will start from the current user and try to build a tree of their friends up to 3 level deep. The steps are as follow.

1. Find their immediate friends (1st degree connection), and filter out all the headings results that belongs to their friends. At this point, if heading results are empty, we can return no results immediately.
2. If heading results are not empty, go into a loop that will build a friendship tree with root node as the current user. Starting from layer 0, which is the root node, we find friends of the current users in layer 0 and whom we haven't visited before. Then, we build nodes connected to users in layer 0.
3. We keep going for layer 1 and 2 with the same logic. Find friends of the current layer and build new nodes connected to current layer's nodes. Along the way, if we found a node with the same value of a user who has the heading we're looking for, we'll store that node and the corresponding heading in a `results` array.
4. After the tree building is finished. We can check the results array to see if we found any user that match the headings' users.
5. We trace back from the nodes we found to find the full path and format the results with the correct order of the path.

This is similar to a breadth first search algorithm, but since we don't have a tree ready at all time, we have to build the tree from the ground up. There are pros and cons to this approach.

Pros:

- We can specify the depth level we want to search.
- Limit the number of queries made to the database since we don't find friends of each individual. We find friends of an entire layer at once. For a depth of 3, we'll issue at most 3 queries.

Cons:

- Since we don't stop immediately when we find a result, we'll need to keep going for all connections of all the layers. This lead to a performance issues when the number of connections increase. Caching can be used to mitigate the issue.
- It's long and hard to read. Future maintainers may have issues when trying to implementing new features or fixing bugs.
