Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users, except: [:destroy]
      resources :relationships, ony: [:show, :create]
      resources :sessions, ony: [:create]

      get "/experts_search", to: "experts_finder#search", as: :experts_search
    end
  end
end
